# 项目说明

此项目为一些学习php的示例代码

# 目录结构说明

* study 学习代码

* action 一些已经写好的功能

* lib 一些已经写好的函数和类库

# 其他说明

* 每个目录下的README.md都会展示演示列表

* 每个演示下面的README.md都会写上环境需求和注意事项

* 涉及到数据库的，一律位于目录下的sql.sql中

* 此演示不包括任何MySQL系列函数及其他将被废弃的函数

* 数据库相关变量，程序从系统定义的常量中读取。实际使用/本地测试的时候请自行修改