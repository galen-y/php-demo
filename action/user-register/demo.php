<?php
require('../../common.php');
header('Content-type:text/html; charset=utf-8'); //强制编码
//判断一下是不是该确认注册了
if (isset($_POST['submit'])) { //该确认用户名和密码
	//从环境变量中取得数据库信息
	$mysql=getMysql();
	$dbhost=$mysql['host'];
	$dbport=$mysql['port'];
	$dbuser=$mysql['user'];
	$dbpass=$mysql['password'];
	$dbname=$mysql['db'];
	//连接数据库
	$link=mysqli_init();
	mysqli_real_connect($link,$dbhost,$dbuser,$dbpass,FALSE,$dbport);
	mysqli_errno($link)!=0 && exit('错误警告： 链接到MySQL发生错误');
	//处理错误，成功连接则选择数据库
	if (!$link) exit('连接数据库失败，可能数据库密码不对或数据库服务器出错！');
	//这两句是用来设置语言，防止乱码的
	mysqli_query($link,"SET character_set_connection=utf8,character_set_results=utf8,character_set_client=binary");
	mysqli_query($link,"SET sql_mode=''");
	echo '<pre>连接数据库成功！',"\n";
	if ($dbname && !@mysqli_select_db($link,$dbname)) exit('无法使用数据库');
	//好了，开始接收用户的数据了
	$user=$_POST['user']; //为了使演示简单，这里不做任何防注入处理
	$password=md5($_POST['password']);
	echo '输入的用户名是：',$user,"\n","密码是：",$_POST['password'],"\n";
	//判断用户名是不是全英文/数字，你可以去掉或者修改
	if (!preg_match('/^[a-zA-Z0-9]+$/',$user)) exit('用户名只能是英文或者数字');
	//判断用户是否已经存在
	$num=@mysqli_fetch_array(mysqli_query($link,"SELECT count(*) as num FROM `demo_user_login` WHERE username='$user'")); //注意as num，对照下面一句你就知道是什么意思了
	if ($num['num']!=0) exit('用户已存在');
	//插入数据
	if (!mysqli_query($link,"INSERT INTO `demo_user_login` (`username`,`password`) VALUES ('$user','$password')")) {
		exit('添加用户失败');
	}
	echo '注册成功，你的ID是：',mysqli_insert_id($link);
} else { //显示登录页面
?>
<form method="post">
	<p>用户名：<input type="text" name="user"></p>
	<p>密码：<input type="text" name="password"></p>
	<p><input type="submit" name="submit" value="注册"></p>
</form>
<?php } ?>