# 一些已经写好的功能

* 用户登录（含Ajax） [演示](http://php8demo.oschina.mopaas.com/action/user-login/demo.php) [Ajax演示](http://php8demo.oschina.mopaas.com/action/user-login/demo-ajax.html) [代码](user-login)

* 用户注册 [演示](http://php8demo.oschina.mopaas.com/action/user-register/demo.php) [代码](user-register)

* 分页（含Ajax） [演示](http://php8demo.oschina.mopaas.com/action/paging/demo.php) [Ajax演示](http://php8demo.oschina.mopaas.com/action/paging/demo-ajax.html) [代码](paging)