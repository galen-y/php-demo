<?php
/*
 * 演示：分页
 * 提示：为了方便查看，这次界面不是那么丑了
 * 用到了Bootstrap这个前端框架。我对这个比较熟悉，所以就用这个
*/
require('../../common.php');
header('Content-type:text/html; charset=utf-8'); //强制编码
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>分页</title>
		<link href="http://cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
		<style>
			#wrapper {
				width:600px;
				margin:0 auto;
			}
		</style>
	</head>
	<body>
		<div id="wrapper">
			<h1>分页演示</h1>
			<h3>基本信息</h3>
			<div class="list-group">
			<?php
				//取得数据库信息
				$mysql=getMysql();
				$dbhost=$mysql['host'];
				$dbport=$mysql['port'];
				$dbuser=$mysql['user'];
				$dbpass=$mysql['password'];
				$dbname=$mysql['db'];
				var_dump($mysql);
				//连接数据库
				$link=mysqli_init();
				mysqli_real_connect($link,$dbhost,$dbuser,$dbpass,FALSE,$dbport);
				mysqli_errno($link)!=0 && exit('错误警告： 链接到MySQL发生错误');
				//处理错误，成功连接则选择数据库
				if (!$link) exit('连接数据库失败，可能数据库密码不对或数据库服务器出错！');
				//这两句是用来设置语言，防止乱码的
				mysqli_query($link,"SET character_set_connection=utf8,character_set_results=utf8,character_set_client=binary");
				mysqli_query($link,"SET sql_mode=''");
				echo '<a class="list-group-item">连接数据库成功！</a>';
				if ($dbname && !@mysqli_select_db($link,$dbname)) exit('无法使用数据库');
				//接收页码
				$page=isset($_GET['page'])?intval($_GET['page']):1; //这句代码的意思是，如果$_GET['page']存在，$page就是intval($_GET['page'])，否侧就是1
				if ($page<=0) $page=1; //防止MySQL那边出错
				$start=($page-1)*10; //我们这里一页10条
				echo '<a class="list-group-item">当前页码：',$page,'</a>';
				//查询数据
				$num=@mysqli_fetch_array(mysqli_query($link,"SELECT count(*) as num FROM `demo_paging`")); //获取文章的数目
				$num=$num['num'];
				if ($num%10===0) $allpage=intval($num/10); //如果$num是10的整数倍，那么一共就有$num/10页
				else $allpage=ceil($num/10); //否则页数就比$num/10大1
				echo '<a class="list-group-item">一共有',$num,'篇文章</a><a class="list-group-item">总计',$allpage,'页</a>';
			?>
			</div>
			<h3>文章列表</h3>
			<div class="list-group">
			<?php
				$r=mysqli_query($link,"SELECT * FROM `demo_paging` ORDER BY id DESC LIMIT $start,10"); //一页10条
				while ($row=mysqli_fetch_array($r)) {
					echo '<a class="list-group-item">',$row['title'],'（',$row['id'],'）</a>';
				}
			?>
			</div>
			<div>
			<?php
				if ($page==1) echo '<a class="btn btn-primary disabled">上一页</a>';
				else echo '<a class="btn btn-primary" href="?page=',$page-1,'">上一页</a>';
				if ($page==$allpage) echo '<a class="btn btn-primary disabled">下一页</a>';
				else echo '<a class="btn btn-primary" href="?page=',$page+1,'">下一页</a>';
			?>
			</div>
		</div>
	</body>
</html>