<?php
require('../../common.php');
header('Content-type:application/json; charset=utf-8'); //强制编码
//从环境变量中取得数据库信息
$mysql=getMysql();
$dbhost=$mysql['host'];
$dbport=$mysql['port'];
$dbuser=$mysql['user'];
$dbpass=$mysql['password'];
$dbname=$mysql['db'];
//连接数据库
$link=mysqli_init();
mysqli_real_connect($link,$dbhost,$dbuser,$dbpass,FALSE,$dbport);
mysqli_errno($link)!=0 && exit('{}');
//处理错误，成功连接则选择数据库
if (!$link) exit('{}');
//这两句是用来设置语言，防止乱码的
mysqli_query($link,"SET character_set_connection=utf8,character_set_results=utf8,character_set_client=binary");
mysqli_query($link,"SET sql_mode=''");
if ($dbname && !@mysqli_select_db($link,$dbname)) exit('{}');
$result=array();
//接收页码
$page=isset($_GET['page'])?intval($_GET['page']):1; //这句代码的意思是，如果$_GET['page']存在，$page就是intval($_GET['page'])，否侧就是1
if ($page<=0) $page=1; //防止MySQL那边出错
$start=($page-1)*10; //我们这里一页10条
//查询数据
$num=@mysqli_fetch_array(mysqli_query($link,"SELECT count(*) as num FROM `demo_paging`")); //获取文章的数目
$num=$num['num'];
if ($num%10===0) $allpage=intval($num/10); //如果$num是10的整数倍，那么一共就有$num/10页
else $allpage=ceil($num/10); //否则页数就比$num/10大1
$result['articlenum']=$num;
$result['pagenum']=$allpage;
$result['article']=array();;
$r=mysqli_query($link,"SELECT * FROM `demo_paging` ORDER BY id DESC LIMIT $start,10"); //一页10条
while ($row=mysqli_fetch_array($r)) {
	$result['article'][]=array('id'=>$row['id'],'title'=>$row['title']);
}
echo json_encode($result);
?>